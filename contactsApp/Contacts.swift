//
//  contacts.swift
//  contactsApp
//
//  Created by Canyon Duncan on 10/12/17.
//  Copyright © 2017 Canyon Duncan. All rights reserved.
//

import Foundation
import RealmSwift

class Contact: Object {
    dynamic var firstName = ""
    dynamic var lastName = ""
    dynamic var phoneNumber = ""
    dynamic var email = ""
    dynamic var address = ""
    dynamic var image = NSData()
    
}
