//
//  showContactViewController.swift
//  contactsApp
//
//  Created by Canyon Duncan on 10/12/17.
//  Copyright © 2017 Canyon Duncan. All rights reserved.
//

import UIKit
import RealmSwift
import MessageUI

class showContactViewController: UIViewController, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate {
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var address: UITextField!
    
    
    //CALL TEXT AND EMAIL FUNCTIONS
    
    @IBAction func makeCall(_ sender: Any) {
        
        let number = phone.text
        if let url = URL(string: "tel://\(String(describing: number))"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        else {
            print("Your device doesn't support this feature.")
        }
    }
    
    @IBAction func textButton(_ sender: Any) {
        let number = phone.text
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = "Message Body"
            controller.recipients = [number!]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        //... handle sms screen actions
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func emailButton(_ sender: Any) {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        
        // Configure the fields of the interface.
        //composeVC.setToRecipients(["ctr89josh@gmail.com"])
        //composeVC.setSubject("StoryBook Feedback")
        //composeVC.setMessageBody("Hey Josh! Here's my feedback.", isHTML: false)
        
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }

    
    
    var selectedContact = Contact()
    var indexAtRow = Int();
    let realm = try! Realm()
    var deleteButton = UIButton()
    var deleteButtonPressed = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //let editButton = UIBarButtonItem(image: nil, style: .plain, target: self, action: Selector("action")) // action:#selector(Class.MethodName) for swift 3
        self.navigationItem.rightBarButtonItem  = editButtonItem
        self.deleteButton = UIButton(frame: CGRect(x: 200, y: 600, width: 100, height: 30))
        self.deleteButton.backgroundColor = UIColor.red
        //self.deleteButton.setTitle("Delete Contact", for: )
        self.deleteButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)

        
        
       firstName.isUserInteractionEnabled = false
       lastName.isUserInteractionEnabled = false
       phone.isUserInteractionEnabled = false
       email.isUserInteractionEnabled = false
       address.isUserInteractionEnabled = false
        
        
        
        firstName.text = selectedContact.firstName
        lastName.text = selectedContact.lastName
        phone.text = selectedContact.phoneNumber
        email.text = selectedContact.email
        address.text = selectedContact.address
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        if(self.deleteButtonPressed == false){
            let contacts = realm.objects(Contact.self)
            try! realm.write {
                contacts[self.indexAtRow].firstName = self.firstName.text!

            }
        }
        else {
            self.deleteButtonPressed = false
        }

            
    }
    
    
    override func setEditing(_ editing: Bool, animated: Bool){

        super.setEditing(editing, animated: animated)
        //self.setEditing(editing, animated: true)
        if editing == true {
            firstName.isUserInteractionEnabled = true
            lastName.isUserInteractionEnabled = true
            phone.isUserInteractionEnabled = true
            email.isUserInteractionEnabled = true
            address.isUserInteractionEnabled = true
            
            firstName.select(Any?.self)
            self.view.addSubview(self.deleteButton)
            
            
        }
        else {
            firstName.isUserInteractionEnabled = false
            lastName.isUserInteractionEnabled = false
            phone.isUserInteractionEnabled = false
            email.isUserInteractionEnabled = false
            address.isUserInteractionEnabled = false
            //self.deleteButton.removeFromSuperview
        }
        
        
    }
    
    
    func buttonAction() {
        //ADD Deleteing the contact
        let contacts = realm.objects(Contact.self)
        
        try! realm.write {
            realm.delete(contacts[self.indexAtRow])
        }
        self.deleteButtonPressed = true
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func fistNameChanged(_ sender: Any) {
        selectedContact.firstName = firstName.text!
    }
    @IBAction func lastNameChanged(_ sender: Any) {
        selectedContact.lastName = lastName.text!
    }
    
    @IBAction func emailChanged(_ sender: Any) {
        selectedContact.email = email.text!
    }
    
    @IBAction func addressChanged(_ sender: Any) {
        selectedContact.address = address.text!
    }
    
    @IBAction func phoneChanged(_ sender: Any) {
        selectedContact.phoneNumber = phone.text!
    }
    
    //This is for the keyboard to GO AWAYY !! when user clicks anywhere on the view
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    //This is for the keyboard to GO AWAYY !! when user clicks "Return" key  on the keyboard
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    
    
    
    
    
    
    
}
