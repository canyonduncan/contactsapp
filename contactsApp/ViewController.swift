//
//  ViewController.swift
//  contactsApp
//
//  Created by Canyon Duncan on 10/12/17.
//  Copyright © 2017 Canyon Duncan. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var selectedContact = Contact()
    var indexAtPath = Int()
    let contacts = try! Realm().objects(Contact.self)
    var updateInContacts : NotificationToken?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateInContacts = notificationUpdate(tasks: contacts)
        self.view.backgroundColor = UIColor.darkGray
        //var contacts = try! Realm().objects(Contact.self)
        //print(self.contacts[0])
        self.tableView.delegate = self
        self.tableView.dataSource = self
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func notificationUpdate (tasks: Results<Contact>) -> NotificationToken {
        return contacts.addNotificationBlock { [weak self] (changes: RealmCollectionChange<Results<Contact>>) in
            self?.updateUI(changes: changes)
        }
    }
    
    func updateUI (changes: RealmCollectionChange<Results<Contact>>) {
        switch changes {
        case .initial(_):
            tableView.reloadData()
        case .update(_, let deletions, let insertions, _):
            tableView.beginUpdates()
            tableView.insertRows(at: insertions.map {(NSIndexPath(row: $0, section: 0) as IndexPath)}, with: .automatic)
            tableView.deleteRows(at: deletions.map {(NSIndexPath(row: $0, section: 0) as IndexPath)}, with: .automatic)
            tableView.endUpdates()
            tableView.reloadData()
            break
        case .error(_):
            print ("error")
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let contact = self.contacts[indexPath.row]
        cell.textLabel!.text = contact.firstName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.selectedContact = self.contacts[indexPath.row]
        self.indexAtPath = indexPath.row
        performSegue(withIdentifier: "showContact", sender: nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showContact"{
            let vcDestination = segue.destination as? showContactViewController
            vcDestination?.selectedContact = self.selectedContact
            vcDestination?.indexAtRow = self.indexAtPath
        }
    }
    
    
    
    

}

